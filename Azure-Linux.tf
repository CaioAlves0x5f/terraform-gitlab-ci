
data "azurerm_resource_group" "RESOURCE-GROUP-VM-TF" {
  name = "RESOURCE-GROUP-TERRAFORM"
}

resource "azurerm_public_ip" "IP-Public-Linux" {
  name                = "IP-Public-Linux"
  location            = var.location
  resource_group_name = data.azurerm_resource_group.RESOURCE-GROUP-VM-TF.name

  allocation_method = "Dynamic"
}

resource "azurerm_network_interface" "Network_Interface" {
  name                = "Network-Interface-TF"
  location            = var.location
  resource_group_name = data.azurerm_resource_group.RESOURCE-GROUP-VM-TF.name

  ip_configuration {
    name                          = "IP-LINUX"
    subnet_id                     = data.terraform_remote_state.vm_network.outputs.Subnet_01_id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.IP-Public-Linux.id
  }
}

resource "azurerm_linux_virtual_machine" "Linux" {
  name                = "LINUX"
  resource_group_name = data.azurerm_resource_group.RESOURCE-GROUP-VM-TF.name
  location            = var.location
  size                = "Standard_B1s"
  admin_username      = "adminuser"
  network_interface_ids = [
    azurerm_network_interface.Network_Interface.id
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file(var.azure_pub_key)
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.5"
    version   = "latest"
  }
}

resource "azurerm_network_interface_security_group_association" "Security-Group-Association" {
  network_interface_id      = azurerm_network_interface.Network_Interface.id
  network_security_group_id = data.terraform_remote_state.vm_network.outputs.Security_Group
}