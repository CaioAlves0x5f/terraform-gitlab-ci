output "AZURE_IP" {
  value = azurerm_public_ip.IP-Public-Linux.ip_address

}

output "AWS_IP" {
  value = aws_instance.VM.public_ip

}