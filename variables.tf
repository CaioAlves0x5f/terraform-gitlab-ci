variable "location" {
  description = "Variavel que indica a região onde os recursos serão criados"
  type        = string
  default     = "Brazil South"
}

variable "aws_pub_key" {
  description = "Chave publica para VM AWS"
  type = string
  
}

variable "azure_pub_key" {
  description = "Chave publica para VM azure"
  type = string
  
}