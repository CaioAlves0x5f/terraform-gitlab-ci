# Terraform GitLab-CI  

## Provisionamento de duas maquinas virtuais (Azure e AWS).

As VMs foram provisionadas em VPC e vNET existente que também foram provisionadas via Terraform e consultadas via Remote States

- [ ] [AWS - REMOTESTATES VPC](https://gitlab.com/CaioAlves0x5f/Terraform/-/tree/master/AWS/REMOTESTATES-VPC)
- [ ] [AZURE - REMOTESTATES VNET](https://gitlab.com/CaioAlves0x5f/Terraform/-/tree/master/AZURE/AZURE-VNET)

O Remotes States foi armazenando em uma Bucket S3 que contém outros projetos implementados
![AWS - BucketS3:](https://gitlab.com/CaioAlves0x5f/terraform-gitlab-ci/-/raw/master/BucketS3.png)


## Add your files

```
cd existing_repo
git remote add origin https://gitlab.com/CaioAlves0x5f/terraform-gitlab-ci.git
git branch -M main
git push -uf origin main
```

