resource "aws_key_pair" "aws_key" {
  key_name   = "aws_key"
  public_key = file(var.aws_pub_key)
}

resource "aws_instance" "VM" {
  ami                         = "ami-037c192f0fa52a358" # sa-east-1b
  instance_type               = "t2.micro"
  key_name                    = aws_key_pair.aws_key.key_name
  subnet_id                   = data.terraform_remote_state.VPC.outputs.subnet_id
  vpc_security_group_ids      = [data.terraform_remote_state.VPC.outputs.security_group_id]
  associate_public_ip_address = true
  #availability_zone = "sa-east-1b"

  tags = {
    Name = "VM-Terraform-Lab"
  }

}