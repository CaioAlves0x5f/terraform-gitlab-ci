terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.15.1"
    }
  }

  backend "s3" {
    bucket = "tf-remotestate-devopslab"
    key    = "Pipeline-GitLab-CI/terraform.tfstate"
    region = "sa-east-1"
  }
}

provider "azurerm" {
  features {}

}

data "terraform_remote_state" "vm_network" {
  backend = "s3"
  config = {
    bucket = "tf-remotestate-devopslab"
    key    = "Azure-VNET/terraform.tfstate"
    region = "sa-east-1"
  }
}

provider "aws" {
  region = "sa-east-1"
}

data "terraform_remote_state" "VPC" {
  backend = "s3"
  config = {
    bucket = "tf-remotestate-devopslab"
    key    = "aws-vpc/terraform.tfstate"
    region = "sa-east-1"
  }
}